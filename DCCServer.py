import operator
import os
import sys
import threading
import select
import socket
import types
import platform
import time


if sys.version_info > (3, 0):
    USE_PY3 = True
    import socketserver as SocketServer
    import queue as Queue
    import pickle as PICKLE

    text_type = str
    user_input = input

else:
    import SocketServer
    import Queue
    import cPickle as PICKLE

    user_input = raw_input
    text_type = unicode

USE_GAFFER = False
USE_MAYA = False
USE_NUKE = False
USE_STANDALONE = False
try: 
    import GafferUI
    USE_GAFFER = True
except:
    try:
        from maya import utils
        USE_MAYA = True
    except:
        try:
            import nuke
            USE_NUKE = True
        except:
            USE_STANDALONE = True


from errno import EINTR

USE_WINDOWS = False
USE_PY3 = False

if platform.system() == "Windows":
    USE_WINDOWS = True
    import ServerRegistryMMap


# map of command ports portName->CommandServer
__commandPorts = {}

# Transfer string encoding
transformEncoding = "utf8"
mayaEncoding = "UTF-8"


class TCommandHandler(SocketServer.StreamRequestHandler):
    # the response terminator
    resp_term = b"\n\x00"

    def receiveData(self):
        """Called by handle() to accumulate one incoming message.  

        Returns the received message, None if the request should be terminated

        Raises socket.timeout if no data is read

        """

        nextdata = self.request.recv(self.server.bufferSize)
        if nextdata is None:
            return None
        data = nextdata.rstrip(b"\x00")
        if len(data) == 0:
            return None
        oldtimeout = self.request.gettimeout()
        self.request.settimeout(1.5)
        while len(nextdata) >= self.server.bufferSize:
            try:
                nextdata = self.request.recv(self.server.bufferSize)
                data += nextdata
            except socket.timeout:
                # no more data, go with what we have
                break
        self.request.settimeout(oldtimeout)

        # First, decode received utf8 string
        try:
            data = data.strip().decode(transformEncoding)
        except:
            sys.stderr.write("Received string is not a valid utf8 string." + "\n")
            return None
        # Second, encode it to the local encoding
        try:
            data = data.encode(mayaEncoding)
        except:
            warnMsg = "Failed to encode string from unicode to %s." % mayaEncoding
            sys.stderr.write(warnMsg + "\n")
            return None

        return data

    def handle(self):
        """Called to handle each connection request
        """

        try:
            # if we are echoing output, we hold on to the first request
            # and poll the socket for pending command messages.
            # Otherwise, we just block on the socket.
            if self.server.echoOutput:
                self.request.settimeout(1.5)
            while not self.server.die:
                # check for pending command messages
                if self.server.echoOutput:
                    while not self.server.commandMessageQueue.empty():
                        self.wfile.write(
                            self.server.commandMessageQueue.get() + self.resp_term
                        )
                # set self.data to be the incoming message
                try:
                    self.data = self.receiveData()
                except socket.timeout:
                    continue
                if self.data is None:
                    break
                # check if we need to display the security warning
                # posting the dialog also has to be done in the gui thread
                # if self.server.securityWarning:
                #     utils.executeInMainThreadWithResult(self.postSecurityWarning)
                #     if self.dialog_result is False:
                #         self.wfile.write(maya.stringTable['y_CommandPort.kExecutionDeniedByMaya' ] + self.resp_term)
                #         return
                #     elif self.dialog_result is True:
                #         self.server.securityWarning = False

                # execute the message
                # response = utils.executeInMainThreadWithResult(self._languageExecute)
                response = self.executeInMainThreadWithResult(self._languageExecute)

                # write the command responses to the client
                self.wfile.write(response)
        except socket.error:
            # a socket exception is a normal way to terminate the connection
            pass

    def executeInMainThreadWithResult(self, fun, *args, **kwargs):
        if USE_MAYA:
            return utils.executeInMainThreadWithResult(fun)
        if USE_GAFFER:
            return GafferUI.EventLoop.executeOnUIThread( fun, waitForResult=True )
        if USE_NUKE:
            return nuke.executeInMainThreadWithResult(fun)

class PythonCommandHandler(TCommandHandler):
    """
    The StreamRequestHandler instance for deferred python execution
    """

    def _languageExecute(self):
        print("LANG_EXEC Received: ")
        print(self.data)
        lines = self.data.splitlines()
        # if there is a prefix, add it to the start of each line
        if len(self.server.prefix) > 0:
            newLines = []
            for _line in lines:
                # newLines.append( self.server.prefix + ' "' + cmds.encodeString(_line) + ' "' )
                newLines.append(self.server.prefix + ' "' + _line + ' "')
            lines = newLines
            self.data = "\n".join(newLines)

        outp = None
        if len(lines) == 1:
            try:
                # first try evaluating it as an expression so we can get the result
                print("LANG_EXEC Trying Eval")
                outp = eval(self.data, globals(), locals())
            except Exception as ex:
                print("LANG_EXEC Eval Failed: {}".format(ex))
        if outp is None:
            # more than one line, or failed to eval
            # exec everything instead
            try:
                print("Trying Exec")
                exec(self.data, globals(), locals())
            except Exception as ex:
                print("Exec Failed")
                outp = ex

        if self.server.returnNbCommands:
            outp = len(lines)
        if not self.server.sendResults:
            outp = ""

        # If it is a local string, we need to decode it to unicode first
        if isinstance(outp, str):
            outp = text_type(outp, mayaEncoding)
        # If it is a list, try to deocode all local strings inside it to unicode
        if type(outp) == type([]):
            listSize = len(outp)
            for i in range(0, listSize):
                if isinstance(outp[i], str):
                    outp[i] = text_type(outp[i], mayaEncoding)

        if self.server.pickleOutput and self.server.sendResults:
            try:
                # Pickle form, type of "outp" is "str"
                outp = text_type(PICKLE.dumps(outp))
            except PICKLE.PickleError as ex:
                outp = text_type(ex)
        elif not isinstance(outp, text_type):
            outp = text_type(outp)

        # Convert Maya encoding to utf8
        if not USE_PY3:
            outp = outp.encode(transformEncoding)
        return outp + self.resp_term


def commandOutputCallback(message, msgType, qu):
    # Command callback function
    # When new output comes in, we add the output
    # to the message queue, which the request handler will drain
    #
    # message - the message string
    # msgType - message type enum
    # qu      - user data, which is the message queue
    qu.put(message)


class CommandServer(threading.Thread):
    """
    Server for one command port
    
    socketServer - StreamServer instance to serve on endpoint
    """

    def __init__(self, socketServer):
        threading.Thread.__init__(self, None)
        self.daemon = True
        self.__servObj = socketServer
        self.__die = False

    def run(self):
        r""" called by Thread.start """
        self.__servObj.die = False
        if self.__servObj.echoOutput:
            self.__servObj.commandMessageQueue = Queue.Queue()
            # self.callbackId = OpenMaya.MCommandMessage.addCommandOutputCallback(commandOutputCallback, self.__servObj.commandMessageQueue)

        while not self.__die:
            try:
                self.__servObj.handle_request()
            except select.error as ex:
                # EINTR error can be ignored
                if ex[0] == EINTR:
                    continue
                raise

    def __del__(self):
        if self.__servObj.echoOutput:
            print("Bye")
            # OpenMaya.MCommandMessage.removeCallback(self.callbackId)
        self.__servObj.cleanup()

    def shutdown(self):
        """ tell the server to shutdown """
        self.__die = True
        try:
            # create a socket, connect to our listening socket and poke it so that it dies
            self.__servObj.die = True
            s = socket.socket(self.__servObj.socket.family, self.__servObj.socket.type)
            s.connect(self.__servObj.endpoint)
            s.send(b"\x00")
            s.close()
        except (socket.gaierror, socket.error):
            # several errors could happen here,
            # most are ok as we are closing the socket anyway
            pass


def openCommandPort(
    portName="127.0.0.1:7001",
    prefix="",
    sendResults=True,
    returnNbCommands=True,
    echoOutput=True,
    bufferSize=4096,
    securityWarning=False,
    pickleOutput=False,
):
    """
    Open a command port with the given name
    Can be INET (host:port) or Unix local format (/tmp/endpoint)
    
    On Windows, the Unix-style format will create a named binary file-mapping
    object which contains a mapping of paths to ports.
    On Mac and Linux a Unix-domain socket will be created.
    
    Environment variable DCC_IP_TYPE can be used to override the default 
    address family by setting it to either 'ipv4' or 'ipv6'.  The default is 
    ipv4.

    portname         - name of the port, used to refer to the server, follows 
                       above format
    prefix           - string prefix to prepend to every command
    sendResults      - True means send results of commands back to client
    returnNbCommands - True means return the number of commands executed
    echoOutput       - True means echo command output to the port
    bufferSize       - byte size of the socket buffer
    securityWarning  - True means issue a security warning
    pickleOutput     - True means the return string for python command would be
                       pickled, default is false
    Returns error string on failure, None on success
    """
    # make sure port name is encodable as str
    try:
        portName = str(portName)
    except UnicodeEncodeError:
        msg = "Could not create command port: %s (name is encodable)."
        return msg % portName

    nameLen = len(portName)

    # parse out an ipv6 network resource identifier format '[2001:db8::1428:57ab]:443' -> '2001:db8::1428:57ab:443'
    if nameLen > 0 and portName[0] == "[" and portName.rfind("]") > 0:
        portNameNormal = portName[1:].replace("]", "")
    else:
        portNameNormal = portName

    portInvalidNameMsg = "Could not create command port: %s (name is invalid)."
    colonIx = portNameNormal.rfind(":")
    if nameLen == 0 or colonIx == nameLen - 1:
        msg = portInvalidNameMsg
        return msg % portName

    # check if the portName is already active
    if portName in __commandPorts:
        msg = "Command port: %s is already active."
        return msg % portName

    # select default address family, defaulting to ipv4 unless we set the environment
    # variable to use IPv6
    # ultimatly, the address family is determined by the portName string, which may be
    # one or the other.
    hasIPV6 = False
    addrFamily = socket.AF_INET
    host = "127.0.0.1"
    try:
        dcc_ip_type = os.environ["DCC_IP_TYPE"].lower()
        if "ipv6" == dcc_ip_type:
            try:
                # this call will raise if the OS doesn't support ipv6
                socket.getaddrinfo("::1", None)
                hasIPV6 = True
                host = "::1"
                addrFamily = socket.AF_INET6
            except socket.gaierror:
                # ipv6 isn't going work
                pass
    except KeyError:
        pass

    port = 0
    # parse out the endpoint
    if colonIx >= 0:
        defaulthost = host
        # INET port, don't know if it is ipv4 or ipv6 yet
        try:
            if colonIx == 0:
                # eg: ':65000'
                port = int(portNameNormal[1:])
            else:
                # eg: 'localhost:65000'
                host = portNameNormal[0:colonIx]
                port = int(portNameNormal[colonIx + 1 :])

            if port > 65535 or port < 1:
                msg = "Could not create command port: %s (port number is invalid)."
                return msg % portName

            try:
                try:
                    # check the endpoint with the current addrFamily and host
                    endpoints = socket.getaddrinfo(host, port, addrFamily)
                except socket.gaierror:
                    if defaulthost != host:
                        # maybe the hostname is wrong, try the default hostname
                        endpoints = socket.getaddrinfo(defaulthost, port, addrFamily)
                    else:
                        raise
            except socket.gaierror:
                # try the other address family, unless there is no other
                if not hasIPV6:
                    raise
                if addrFamily is socket.AF_INET6:
                    addrFamily = socket.AF_INET
                else:
                    addrFamily = socket.AF_INET6
                endpoints = socket.getaddrinfo(host, port, addrFamily)
            if len(endpoints) == 0:
                raise ValueError
            endpoint = endpoints[0][-1]
        except (ValueError, socket.gaierror):
            msg = portInvalidNameMsg
            return msg % portName
        ServerClass = SocketServer.TCPServer
        ServerClass.address_family = addrFamily

        def cleanup():
            pass

    else:
        # filename (no ':' in name)
        if USE_WINDOWS:
            try:
                # We have a named mmap file wherein we keep the mappings of paths to INET ports
                endpoint = ServerRegistryMMap.registerServer(portName, addrFamily)
            except RuntimeError as ex:
                return ex.args[0]
            ServerClass = SocketServer.TCPServer
            ServerClass.address_family = addrFamily

            def cleanup():
                pass

        else:
            # normal Unix domain socket
            if portNameNormal[0] == "/":
                endpoint = portNameNormal
            else:
                endpoint = "/tmp/" + portNameNormal
            if os.path.exists(endpoint):
                # file is already there, delete it now so that we can create it again
                try:
                    os.remove(endpoint)
                except OSError:
                    # probably won't be able to create the socket
                    # but try anyway
                    pass
            ServerClass = SocketServer.UnixStreamServer

            def cleanup():
                try:
                    os.remove(endpoint)
                except OSError:
                    pass

    try:
        # create the server and attach preferences variables
        socketServer = ServerClass(endpoint, PythonCommandHandler)
        socketServer.prefix = prefix
        socketServer.sendResults = sendResults
        socketServer.returnNbCommands = returnNbCommands and sendResults
        socketServer.echoOutput = echoOutput and sendResults
        socketServer.bufferSize = max(16, int(bufferSize))
        socketServer.securityWarning = securityWarning
        socketServer.portName = portNameNormal
        socketServer.endpoint = endpoint
        socketServer.cleanup = cleanup
        socketServer.pickleOutput = pickleOutput

        # create the command server and start it
        cmdServer = CommandServer(socketServer)
        __commandPorts[portName] = cmdServer
        cmdServer.start()
        time.sleep(0)
    except socket.error as ex:
        msg = "Socket error creating command port %s (%s)."

        return msg % (portName, ex.args[0])


def closeCommandPort(portName="127.0.0.1:7001"):
    """ Close the specified command port  
    Returns error string on failure, None on success

    Args:
        portName (str, optional): Hostname and Port to close. Defaults to "127.0.0.1:7001".
    """
    try:
        portName = str(portName)
        server = __commandPorts.pop(portName)
        server.shutdown()
        if USE_WINDOWS:
            activeServers = ServerRegistryMMap.getInstance()
            activeServers.removeServer(portName)
    except (KeyError, UnicodeEncodeError):
        return "Command port %s does not exist." % repr(portName)


def listCommandPorts():
    """
    Returns the list of command port names, in the format used by openCommandPort
    """
    return __commandPorts.keys()


if __name__ == "__main__":
    host = "127.0.0.1"
    port = 7001
    openCommandPort("%(host)s:%(port)s" % locals())
    print("Listening. Press q to quit")

    current = str(user_input())
    while current != "q":
        current = str(user_input())

    closeCommandPort("%(host)s:%(port)s" % locals())
    print("Quiting Server")
