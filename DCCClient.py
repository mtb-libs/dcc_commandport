from telnetlib import Telnet

host = "127.0.0.1"
port = 7001

c = Telnet(host, int(port), timeout=3)
c.write("a=4;print(a*2)".encode("utf8"))
c.close()
