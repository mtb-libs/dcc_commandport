# MTB DCC Command Port

Replicates maya's command port for other DCCs.  
Written in Python 3 with a compatibility layer for Python 2.

Tested in:
- Houdini 18
- Maya 2016 - 2020
- Gaffer
- Blender
- Nuke 12
